# Lite Header & Footer
A standalone header & footer based off of the Lite Template designed to update very outdated sites with modern WWU brand styles.  

## Description
This repository is a stripped down version of the Lite Template with only the styles needed to theme the header and footer.

## Installation
To get up and running, clone the repo down and in any command line.  Make sure you have Ruby and Compass installed.  Once finished, run:
```
compass compile
```
to generate the western.css file.  
