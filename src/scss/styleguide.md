# WWU CSS Documentation

>This site uses [KSS-node](https://github.com/kss-node/kss-node) to automatically generate documentation from SCSS files.  
>
>The goal for this is to have detailed, class-based documentation availible for users of the Western Template who aren't afraid to get their hands dirty with code.  

## Set Up (Mac OS/Unix)
First, install Node.js onto your machine from their website [http://nodejs.org/](http://nodejs.org/).  Then, open a bash shell and enter:
```bash
$ sudo npm install -g kss
```
You'll get lots of errors if you don't use sudo to install the module.  If the install is successful, then you are ready to build the styleguide.  


## Building the site

Currently, the build command is: 
```bash
$ kss-node scss styleguide --css css/main.css --template wwu-kss-template
```

It takes the files in the "scss" directory, parses them and outputs it to the folder "kss-site."  The "-css" command is needed to provide the examples with the compiled CSS.  Plus it looks cooler.  

The --template provides a custom template to build the site off of.  It's completely customizable, but it's currently only needed to make KSS compatible with the WWU Lite CSS file.  

The rough syntax is:
```bash
$ kss-node {source dir} {dest dir} --css {path to additional css} --template {path-to-custom-template}
```


## Syntax and instructions

Put the code comment block right before the first class you want to document.  The classes **must** be in the same order the classes in the comments are or it will not work right.  

You can have HTML markup without example css classes but not the other way around.  

```
//Section Title
//
//Section description.  Multiple lines work too.
//Just have a new line after them.
//
//Markup: <button class="button {$modifiers}">
//
//.class - description of class here
//:hover - also supports subclasses
//
//Styleguide 1.2.3
```

## Pages and References

[Github](https://github.com/kss-node/kss-node)

[KSS Website](http://warpspire.com/kss/syntax/)

